NESTED PERMISSION
----------


INTRODUCTION
------------
	This module provide a functionality to nesting roles permissions. Drag and drop functionality to adjust the roles .

REQUIREMENTS
------------

INSTALLATION
------------


CONFIGURATION
-------------
	1. Go to admin/people and click on the Roles tab.

    2. Drag and Drop the roles.


MAINTAINERS
-----------
Supporting by :
	https://www.drupal.org/u/ankitjaince
